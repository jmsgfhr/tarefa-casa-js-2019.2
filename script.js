let bookListItem = document.querySelector('#book-list');
const addBookButton = document.querySelector('#add-book button');
let addBookInput = document.querySelector("#add-book input");
let newBookListItem = document.querySelector('#book-list ul');

addBookButton.addEventListener('click', (e) => {
    e.preventDefault();
    let newBookItem = document.createElement('li');
    let newBookButton = document.createElement('span');
    newBookButton.classList.add('delete');
    newBookButton.innerText = "excluir"
    let newBookText = document.createElement('span');
    newBookText.innerText = addBookInput.value;
    newBookText.classList.add('name');
    newBookItem.appendChild(newBookButton);
    newBookItem.appendChild(newBookText);
    newBookListItem.appendChild(newBookItem);
});

newBookListItem.addEventListener('click', (e) => {
    if(e.target.nodeName==='SPAN' && e.target.className === 'delete') e.target.parentNode.remove();
});